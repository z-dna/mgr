import sys, json;

keys = []

with open("exp_range_results.json") as f:
    data = json.load(f)
    for key in data:
        keys.append(float(key))

    top_1 = []
    top_10 = []

    keys.sort()
    for key in keys:
        top_1.append(data[str(key)]["rank"]["1"])
        top_10.append(data[str(key)]["rank"]["10"]+data[str(key)]["rank"]["1"])

    print(top_1)
    print(top_10)