package pl.intelliseq.explorare.model.hpo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static pl.intelliseq.explorare.model.hpo.HpoOboParser.PATH_TO_DISEASES_FILE;
import static pl.intelliseq.explorare.model.hpo.HpoOboParser.PATH_TO_PHENOTYPE_TO_GENES_FILE;

public class DiseaseGeneDictionary {
    private Map<String, Disease> diseases = new TreeMap<String, Disease>();

    public DiseaseGeneDictionary() {
        this.parseDiseasesFile();
        this.parseGenesFile();
    }

    public boolean add(Disease disease) {
        if (diseases.containsKey(disease.getId())) return false;
        this.diseases.put(disease.getId(), disease);
        return true;
    }

    public Disease getDiseaseById(String id) {
        return this.diseases.get(id);
    }

    public Disease getDiseaseByName(String diseaseName) {
        return this.diseases.values().stream()
                .filter(disease -> disease.getPrimaryName().equals(diseaseName))
                .findFirst()
                .get();
    }

    public Set<String> getDiseases() {
        return this.diseases.entrySet().stream()
                .map(Map.Entry::getValue)
                .map(Disease::getPrimaryName)
                .collect(Collectors.toSet());
    }

    private void parseDiseasesFile() {
        try (Stream<String> stream = Files.lines(Paths.get(PATH_TO_DISEASES_FILE))) {

            stream.forEach(s -> {
                String[] elements = s.split("\t");
                String diseaseId = elements[5];
                Disease disease = new Disease(diseaseId);
                if (this.add(disease)) {
                    for (String diseaseName : elements[2].split(";")) {
                        diseaseName = diseaseName.replaceAll("^?[0-9]{6}\\s", "")
                                .replaceAll("[\\*|#|%|\\+]", "");
                        if (!diseaseName.contains("MOVED TO")) {
                            disease.setPrimaryName(diseaseName);
                            break;
                        }
                    }
                    disease.setSynonyms(
                            new HashSet<String>(Arrays.asList(elements[2].split(";"))));
                    if ((disease.getPrimaryName() == null))
                        this.diseases.remove(diseaseId);
                }
            });

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void parseGenesFile() {
        try (Stream<String> stream = Files.lines(Paths.get(PATH_TO_PHENOTYPE_TO_GENES_FILE))) {

            stream.forEach(s -> {
                if (s.startsWith("#")) return;
                String[] elements = s.split("\t");
                Disease disease = this.getDiseaseById(elements[6]);
                if (disease != null) disease.addGene(elements[3]);
            });

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
