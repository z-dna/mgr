package pl.intelliseq.explorare.model.hpo;

import lombok.extern.log4j.Log4j2;
import pl.intelliseq.explorare.model.enums.ParentSystem;
import pl.intelliseq.explorare.model.enums.RelatednessSystem;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class HpoTree {

    private Map<String, HpoTerm> hpoMap = new TreeMap<>();

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }


    public HpoTree() {
        HpoTerm root = new HpoTerm("HP:0000001");
        hpoMap.put(root.getId(), root);
    }

    public void add(List<String> lineBuffer) {

        HpoTerm term = null;
        for (String line : lineBuffer) {

            String[] elements = line.split(": ");

            if (elements[0].equals("id"))
                term = this.getOrCreateHpoTermById(elements[1]);

            if (elements[0].equals("name"))
                term.setName(elements[1]);

            if (elements[0].equals("synonym"))
                term.addSynonym(elements[1].split("\"")[1]);

            if (elements[0].equals("is_a"))
                term.setParent(
                        this.getOrCreateHpoTermById(elements[1].split(" ")[0])
                );

        } // end parsing line from lineBuffer
    }

    private HpoTerm getOrCreateHpoTermById(String id, Boolean create) {

        if (hpoMap.containsKey(id)) return hpoMap.get(id);

        if (create) {
            HpoTerm newTerm = new HpoTerm(id);
            hpoMap.put(id, newTerm);
            return newTerm;
        }

        throw new RuntimeException("No such HPO term " + id);
    }

    public HpoTerm getOrCreateHpoTermById(String id) {
        return this.getOrCreateHpoTermById(id, true);
    }

    public HpoTerm getHpoTermById(String id) {
        return this.getOrCreateHpoTermById(id, false);
    }

    public void calculateWeights() {
        System.out.println("Starting calculating weights");
        this.getHpoTermById("HP:0000001").setWeight(Setup.WEIGHT_SYSTEM, 1D, Setup.BASE_WEIGHT);
        System.out.println(String.format("Weights calculated, root weight: %f", this.getHpoTermById("HP:0000001").getWeight()));
    }


    public List<HpoTerm> getTerms() {
        return new ArrayList<HpoTerm>(hpoMap.values());
    }

    public List<HpoTerm> getTermsForOboParsing() {
        List<HpoTerm> termsForParsing = new ArrayList<HpoTerm>();
        for (HpoTerm term : hpoMap.values())
            if (term.hasParent("HP:0000118"))
                termsForParsing.add(term);
        return termsForParsing;
    }

    public void addGene(String id, String gene) {
        HpoTerm term = this.getHpoTermById(id);
        term.addGene(gene);
    }

    public void addDisease(String id, String disease) {
        HpoTerm term = this.getHpoTermById(id);
        term.addDisease(disease);
    }

    public Map<HpoTerm, Double> getNormalizedRelatednessCoefficient(Set<HpoTerm> terms) {
        Map<HpoTerm, Double> scaledDistMap = new LinkedHashMap<>();

        if (terms.size() == 1) {
            for (HpoTerm t1 : terms) {
                scaledDistMap.put(t1, 1D);
            }
            return scaledDistMap;
        }

        Double globalSum = 0D;
        for (HpoTerm t1 : terms) {
            Double localSum = 0D;
            for (HpoTerm t2 : terms) {
                if (t1 != t2) {
                    localSum += t1.getDistFromOther(t2);
                }
            }
            scaledDistMap.put(t1, localSum);
            globalSum += localSum;
        }
        globalSum = globalSum / terms.size();

        for (HpoTerm t1 : terms) {
            if (Setup.RELATEDNESS_SYSTEM == RelatednessSystem.SCALED) {
                scaledDistMap.replace(t1, scaledDistMap.get(t1) / globalSum);
            } else if (Setup.RELATEDNESS_SYSTEM == RelatednessSystem.REVERSED) {
                scaledDistMap.replace(t1, globalSum / scaledDistMap.get(t1));
            }
        }

        return scaledDistMap;
    }

    public Map<String, Double> getDiseases(Set<HpoTerm> hpoTerms) {

        Map<String, Double> nonNormalizedDiseaseScores = new LinkedHashMap<String, Double>();
        Map<HpoTerm, Double> normalizedRelatednessCoefficient = null;

        if (Setup.RELATEDNESS_SYSTEM != RelatednessSystem.NONE) {
            normalizedRelatednessCoefficient = getNormalizedRelatednessCoefficient(hpoTerms);
        }

        for (HpoTerm symptom : hpoTerms) {


            Double weight = 1D;

            if (Setup.RELATEDNESS_SYSTEM != RelatednessSystem.NONE) {
//                log.info("RELATEDNESS_SYSTEM = "+Setup.RELATEDNESS_SYSTEM.toString());
                weight = normalizedRelatednessCoefficient.get(symptom);
            }

            symptom.increaseDiseaseScoresByWeight(nonNormalizedDiseaseScores, weight);

//            log.info("BEFORE PARENTS");


            Double finalWeight = weight;

            if (Setup.PARENT_SYSTEM.ordinal() >= ParentSystem.FIRST_TIER_PARENTS.ordinal()) {
//                log.info("FIRST_TIER_PARENTS");
                try {
                    symptom.getParents().stream().forEach(
                            p -> p.increaseDiseaseScoresByWeight(nonNormalizedDiseaseScores, Setup.PARENTS_WEIGHTS.get(0) * finalWeight)
                    );
                } catch (Exception e) {
                    log.error(e);
                }
            }

            if (Setup.PARENT_SYSTEM.ordinal() >= ParentSystem.SECOND_TIER_PARENTS.ordinal()) {
//                log.info("SECOND_TIER_PARENTS");
                try {
                    symptom.getParents().stream().flatMap(p -> p.getParents().stream()).forEach(
                            p -> p.increaseDiseaseScoresByWeight(nonNormalizedDiseaseScores, Setup.PARENTS_WEIGHTS.get(1) * finalWeight)
                    );
                } catch (Exception e) {
                    log.error(e);
                }
            }

            if (Setup.PARENT_SYSTEM.ordinal() >= ParentSystem.THIRD_TIER_PARENTS.ordinal()) {
//                log.info("THIRD_TIER_PARENTS");
                try {
                    symptom.getParents().stream().flatMap(p -> p.getParents().stream()).flatMap(p -> p.getParents().stream()).forEach(
                            p -> p.increaseDiseaseScoresByWeight(nonNormalizedDiseaseScores, Setup.PARENTS_WEIGHTS.get(2) * finalWeight)
                    );
                } catch (Exception e) {
                    log.error(e);
                }
            }
        }

        Map<String, Double> sortedMap = HpoTree.sortByValue(nonNormalizedDiseaseScores);

        /* normalize sorted map */
        Double maximum = sortedMap.remove("maximum");
        sortedMap.entrySet().forEach(entry -> entry.setValue(entry.getValue() / maximum));

        return sortedMap;

    }

    public Set<HpoTerm> getPhenotypesByDisease(String diseaseName) {
        Set<HpoTerm> hpoTerms = new HashSet<HpoTerm>();
        for (HpoTerm term : this.getTerms()) {
            if (term.causeDisease(diseaseName))
                hpoTerms.add(term);
        }
        return hpoTerms;
    }


}
