package pl.intelliseq.explorare.model.hpo.results;

import com.fasterxml.jackson.annotation.JsonView;
import pl.intelliseq.explorare.model.hpo.HpoTerm;
import pl.intelliseq.explorare.utils.json.Views;

import java.util.List;

public class PhenotypeResult {

    @JsonView(Views.Rest.class)
    private HpoTerm phenotype;

    @JsonView(Views.Rest.class)
    private List<HpoTerm> parents;

    @JsonView(Views.Rest.class)
    private List<HpoTerm> children;

    public PhenotypeResult(HpoTerm phenotype) {
        this.phenotype = phenotype;
        this.parents = phenotype.getParents();
        this.children = phenotype.getChildren();
    }

    public HpoTerm getPhenotype() {
        return phenotype;
    }

    public List<HpoTerm> getParents() {
        return parents;
    }

    public List<HpoTerm> getChildren() {
        return children;
    }

}
