package pl.intelliseq.explorare.model.hpo.results;

import com.fasterxml.jackson.annotation.JsonView;
import pl.intelliseq.explorare.utils.json.Views;

import java.util.HashSet;
import java.util.Set;

public class Results {

    @JsonView(Views.Rest.class)
    Set<Object> results = new HashSet<Object>();

    public Set<Object> getResults() {
        return results;
    }

    public void addResult(Object result) {
        this.results.add(result);
    }

    public boolean sizeGreaterOrEqualTo(int count) {
        return results.size() >= count;
    }

}
