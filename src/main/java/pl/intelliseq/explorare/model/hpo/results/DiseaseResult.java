package pl.intelliseq.explorare.model.hpo.results;

public class DiseaseResult implements Comparable {
    private String name;
    private Double score;

    public DiseaseResult(String name, Double score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public Double getScore() {
        return score;
    }

    @Override
    public int compareTo(Object o) {
        int compareResult = -this.score.compareTo(((DiseaseResult) o).getScore());
        if (compareResult != 0) return compareResult;
        else return this.name.compareTo(((DiseaseResult) o).getName());
    }
}
