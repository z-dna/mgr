package pl.intelliseq.explorare.model.hpo;

import pl.intelliseq.explorare.model.enums.ParentSystem;
import pl.intelliseq.explorare.model.enums.RelatednessSystem;
import pl.intelliseq.explorare.model.enums.WeightSystem;

import java.util.Collections;
import java.util.List;

public class Setup {

    public static final Double BASE_WEIGHT = 1D;
    public static final Double DEPTH_WEIGHT = 1D;

    public static final WeightSystem WEIGHT_SYSTEM = WeightSystem.PLAIN;

//        public static final List<Double> PARENTS_WEIGHTS = List.of(.5D, .25D, .125D);
//        public static final List<Double> PARENTS_WEIGHTS = List.of(1D, 1D, 1D);
    public static final List<Double> PARENTS_WEIGHTS = Collections.emptyList();

    public static final ParentSystem PARENT_SYSTEM = ParentSystem.NO_PARENTS;

    public static final RelatednessSystem RELATEDNESS_SYSTEM = RelatednessSystem.NONE;

    public static String getDesc() {
        return String.format("BW: %s, WS: %s, DW: %s, PS: %s, PW: %s, RS: %s",
                BASE_WEIGHT.toString(),
                WEIGHT_SYSTEM.toString(),
                DEPTH_WEIGHT.toString(),
                PARENT_SYSTEM.toString(),
                PARENTS_WEIGHTS.toString(),
                RELATEDNESS_SYSTEM.toString());
    }
}
