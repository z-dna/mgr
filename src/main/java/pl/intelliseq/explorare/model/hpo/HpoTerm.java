package pl.intelliseq.explorare.model.hpo;

import com.fasterxml.jackson.annotation.JsonView;
import info.debatty.java.stringsimilarity.Damerau;
import pl.intelliseq.explorare.model.enums.WeightSystem;
import pl.intelliseq.explorare.utils.json.Views;

import java.util.*;

import static java.lang.Math.exp;
import static java.lang.Math.log;

public class HpoTerm {

    @JsonView(Views.Rest.class)
    private String id;
    @JsonView(Views.Rest.class)
    private String name;
    private List<String> synonyms;
    private List<HpoTerm> parents;
    private List<HpoTerm> children;
    private Set<String> genes;
    private Set<String> diseases;
    private Double weight;

    public HpoTerm() {
        this.id = "";
        this.name = "";
        this.weight = 0D;
        this.synonyms = new ArrayList<String>();
        this.children = new ArrayList<HpoTerm>();
        this.genes = new HashSet<String>();
        this.diseases = new HashSet<String>();
        this.parents = new ArrayList<HpoTerm>();
    }

    public HpoTerm(String id) {
        this();

        if (!id.matches("HP:[0-9]{7}"))
            throw new RuntimeException("Wrong id format: " + id);
        this.id = id;

    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParent(HpoTerm parent) {
        parent.addChild(this);
        parents.add(parent);
    }

    public void addSynonym(String synonym) {
        this.synonyms.add(synonym);
    }

    public void addChild(HpoTerm child) {
        this.children.add(child);
    }

    public void addGene(String gene) {
        this.genes.add(gene);
        parents.forEach(p -> p.addGene(gene));
    }

    public void addDisease(String disease) {
        this.diseases.add(disease);
        parents.forEach(p -> p.addDisease(disease));
    }

    public Double setWeight(WeightSystem weightSystem, Double multiplier, Double weight) {
        Double newWeight = null;
        switch (weightSystem) {
            case PLAIN:
                newWeight = weight;
                break;
            case UPPER_DEPTH:
                newWeight = weight + Setup.DEPTH_WEIGHT;
                break;
            case UPPER_DEPTH_LOG:
                newWeight = weight + Setup.DEPTH_WEIGHT;
                if (parents.size() > 0) {
                    multiplier = multiplier / parents.size();
                    this.weight += log(newWeight) * multiplier;
                } else {
                    this.weight = log(newWeight);
                }
                for (HpoTerm child : children) {
                    child.setWeight(weightSystem, multiplier, newWeight);
                }
                return null;
            case UPPER_DEPTH_EXP:
                newWeight = weight + Setup.DEPTH_WEIGHT;
                if (parents.size() > 0) {
                    multiplier = multiplier / parents.size();
                    this.weight += exp(newWeight) * multiplier;
                } else {
                    this.weight = exp(newWeight);
                }
                for (HpoTerm child : children) {
                    child.setWeight(weightSystem, multiplier, newWeight);
                }
                return null;
            case REVERSE:
                if (children.size() == 0) {
                    this.weight = 1D / weight;
                    return weight;
                } else {
                    double tmp = 0;
                    for (HpoTerm child : children) {
                        tmp += child.setWeight(WeightSystem.REVERSE, 0D, weight);
                    }
                    tmp = tmp / children.size() + 1;
                    this.weight = 1 / tmp;
                    return tmp;
                }
            case CHILDREN_COUNT:
                newWeight = weight + children.size();
                break;
            case CHILDREN_COUNT_HALF:
                newWeight = weight + (children.size() / 2);
                break;
            case CHILDREN_COUNT_LOG:
                newWeight = weight + log(children.size() + 1);
                break;
            default:
                throw new RuntimeException("Wrong weight system exception");

        }

        if (parents.size() > 0) {
            multiplier = multiplier / parents.size();
            this.weight += newWeight * multiplier;
        } else {
            this.weight = newWeight;
        }
        for (HpoTerm child : children) {
            child.setWeight(weightSystem, multiplier, newWeight);
        }

        return null;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<String> getGenes() {
        return genes;
    }

    public Set<String> getDiseases() {
        return diseases;
    }

    public List<HpoTerm> getChildren() {
        return children;
    }

    public List<HpoTerm> getParents() {
        return this.parents;
    }

    public double getWeight() {
        return this.weight;
    }


    public boolean isEqualTo(String phrase) {
        return isSimilarTo(phrase, 0);
    }

    public boolean isSimilarTo(String phrase) {
        return isSimilarTo(phrase, 5);
    }

    public boolean isSimilarTo(String phrase, Integer allowedBasesPerMismatch) {

        Integer allowedMismatches = 0;
        if (allowedBasesPerMismatch > 0)
            allowedMismatches = (int) (((float) phrase.length()) / ((float) allowedBasesPerMismatch));
        //System.out.println(phrase + allowedMismatches);

        phrase = phrase.replaceAll("[^a-zA-Z ]", "").toLowerCase();
        try {
            if (phrase.equals(name.toLowerCase())) return true;
        } catch (Exception e) {
            System.out.println(phrase);
            System.out.println(this.id);
            System.out.println(this.name);
        }

        if (allowedMismatches > 0 && phrase.startsWith(name.toLowerCase().substring(0, 3))) {
            Damerau distanceCalculator = new Damerau();
            double distance = distanceCalculator.distance(phrase, name.toLowerCase());
            if (distance <= allowedMismatches) return true;
        }

        if (synonyms != null && synonyms.size() > 0)
            for (String synonym : synonyms)
                if (phrase.equals(synonym.toLowerCase())) return true;

        return false;

    }


    public void increaseDiseaseScoresByWeight(Map<String, Double> diseaseScores, Double weight) {

        if (this.weight == null) {
            System.out.println(id + " " + name);
            return;
        }

        double increaseBy = this.weight * weight;

        for (String disease : this.diseases) {
            if (disease.equals("OMIM:601076")) {
//				System.out.println("DIS OMIM:601076 result: "+this.weight+" "+weight);
            }
            if (diseaseScores.containsKey(disease))
                diseaseScores.put(disease, diseaseScores.get(disease) + increaseBy);
            else
                diseaseScores.put(disease, increaseBy);
        }

        if (diseaseScores.containsKey("maximum"))
            diseaseScores.put("maximum", diseaseScores.get("maximum") + increaseBy);
        else
            diseaseScores.put("maximum", increaseBy);

    }

    public void multiplyDiseaseScoresByWeight(Map<String, Double> diseaseScores, Double weight) {

        if (this.weight == null) {
            System.out.println(id + " " + name);
            return;
        }

        for (String disease : this.diseases)
            if (diseaseScores.containsKey(disease))
                diseaseScores.put(disease, diseaseScores.get(disease) * weight);
            else
                throw new RuntimeException("No disease to multiply");

        if (diseaseScores.containsKey("maximum"))
            diseaseScores.put("maximum", diseaseScores.get("maximum") * weight);
        else
            throw new RuntimeException("No disease to multiply");

    }

    public boolean causeDisease(String diseaseName) {
        for (String disease : this.diseases)
            if (disease.equals(diseaseName))
                return true;
        return false;
    }

    public boolean isChildOf(HpoTerm term) {
        if (parents.size() == 0) return false;
        if (parents.stream().anyMatch(p -> p.equals(term))) {
            return true;
        }
        return parents.stream().anyMatch(p -> p.isChildOf(term));

    }


    public boolean hasParent(String parentId) {
        if (parents.size() == 0) return false;

        if (parents.stream().anyMatch(p -> p.getId().equals(parentId))) {
            return true;
        }
        return parents.stream().anyMatch(p -> p.hasParent(parentId));
    }

    public Map<String, Integer> getPathToRoot(Integer height, Map<String, Integer> map) {
        if (parents.size() == 0) {
            return map;
        } else {
            for (HpoTerm p : parents) {
                if (map.containsKey(p.getId()) && map.get(p.getId()) > height + 1) {
                    map.put(p.getId(), height + 1);
                } else {
                    map.putIfAbsent(p.getId(), height + 1);
                }

                p.getPathToRoot(height + 1, map);
            }
        }
        return map;
    }

    public Integer getDistFromOther(HpoTerm other) {
        Map<String, Integer> t1 = getPathToRoot(0, new HashMap<>());
        Map<String, Integer> t2 = other.getPathToRoot(0, new HashMap<>());
        Integer min = Integer.MAX_VALUE;

        for (String term : t1.keySet()) {
            if (t2.containsKey(term)) {
                if (t1.get(term) + t2.get(term) < min) {
                    min = t1.get(term) + t2.get(term);
                }
            }
        }
        return min;
    }


    @Override
    public String toString() {
        return id + " : " + name;
    }


}
