package pl.intelliseq.explorare.model.enums;

public enum RelatednessSystem {
    SCALED, REVERSED, NONE
}
