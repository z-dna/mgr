package pl.intelliseq.explorare.model.enums;

public enum WeightSystem {
    PLAIN,

    UPPER_DEPTH,
    UPPER_DEPTH_LOG,
    UPPER_DEPTH_EXP,

    REVERSE,

    CHILDREN_COUNT,
    CHILDREN_COUNT_HALF,
    CHILDREN_COUNT_LOG
}
