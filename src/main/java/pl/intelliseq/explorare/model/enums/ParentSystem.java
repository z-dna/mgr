package pl.intelliseq.explorare.model.enums;

public enum ParentSystem {
    NO_PARENTS, FIRST_TIER_PARENTS, SECOND_TIER_PARENTS, THIRD_TIER_PARENTS
}
