package pl.intelliseq.explorare.model;

import java.util.Set;

public class GetGenesRequest {
    private Set<String> hpoTerms;
    private Double threshold = 0.3d;

    public Set<String> getHpoTerms(){
        return hpoTerms;
    }

    public void setHpoTerms(Set<String> hpoTerms) {
        this.hpoTerms = hpoTerms;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

}
