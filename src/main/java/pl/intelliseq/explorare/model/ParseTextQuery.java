package pl.intelliseq.explorare.model;

public class ParseTextQuery {
    private String query;

    public ParseTextQuery() {
        this.query = "";
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}