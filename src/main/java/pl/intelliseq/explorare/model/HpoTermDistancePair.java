package pl.intelliseq.explorare.model;

import pl.intelliseq.explorare.model.hpo.HpoTerm;

import java.util.Objects;

public class HpoTermDistancePair {
    private HpoTerm term1;
    private HpoTerm term2;
    private Integer dist;

    public HpoTermDistancePair(HpoTerm term1, HpoTerm term2, Integer dist) {
        this.term1 = term1;
        this.term2 = term2;
        this.dist = dist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HpoTermDistancePair that = (HpoTermDistancePair) o;
        return (Objects.equals(term1, that.term1) && Objects.equals(term2, that.term2)) || (Objects.equals(term1, that.term2) && Objects.equals(term2, that.term1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(term1.hashCode() + term2.hashCode());
    }

    @Override
    public String toString() {
        return "HpoTermDistancePair{" +
                "term1=" + term1 +
                ", term2=" + term2 +
                ", dist=" + dist +
                '}';
    }
}
