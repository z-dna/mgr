package pl.intelliseq.explorare.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.intelliseq.explorare.model.GetGenesRequest;
import pl.intelliseq.explorare.model.ParseTextQuery;
import pl.intelliseq.explorare.model.hpo.results.DiseaseResult;
import pl.intelliseq.explorare.model.hpo.results.GeneResult;
import pl.intelliseq.explorare.model.hpo.results.PhenotypeResult;
import pl.intelliseq.explorare.model.hpo.results.Results;
import pl.intelliseq.explorare.model.hpo.*;
import pl.intelliseq.explorare.model.phenoMarks.PhenoMarks;
import pl.intelliseq.explorare.model.phenoMarks.PhenoMarksParser;
import pl.intelliseq.explorare.utils.json.Views;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@RestController
public class ExplorareRestController {

    @Autowired
    private PhenoMarksParser phenoMarksParser;

    @Autowired
    private HpoTree hpoTree ;

    @Autowired
    private DiseaseGeneDictionary diseaseGeneDictionary;

    private Set<HpoTerm> makeSetSpecific(Set<HpoTerm> hpoTerms) {
        Set<HpoTerm> out = new HashSet<HpoTerm>();
        for (HpoTerm term : hpoTerms) {
            boolean hasChildren = false;
            for (HpoTerm childTerm : hpoTerms) {
                if (childTerm.isChildOf(term))
                    hasChildren = true;
            }
            if (!hasChildren)
                out.add(term);
        }
        return out;
    }

    @CrossOrigin()
    @JsonView(Views.Rest.class)
    @RequestMapping(path = "/hello", method = RequestMethod.GET, produces = "application/json")
    public String greeting() {
        return new JSONObject().put("response", "explorare service is alive").toString();
    }

    @CrossOrigin
    @RequestMapping(path = "/get-setup", method = RequestMethod.GET)
    public String getSetup() {
        return Setup.getDesc();
    }

    @CrossOrigin()
    @JsonView(Views.Rest.class)
    @RequestMapping(path = "/get-children", method = RequestMethod.GET, produces = "application/json")
    public JSONArray getChildren(@RequestParam(name = "id") String hpoId) {
        HpoTerm term = hpoTree.getHpoTermById(hpoId);

        Set<HpoTerm> set = new HashSet<>();
        LinkedList<HpoTerm> list = new LinkedList<>();
        list.add(term);
        while (list.size() > 0) {
            term = list.pop();
            set.add(term);
            list.addAll(term.getChildren());
        }

        JSONArray out = new JSONArray();
        set.forEach(t -> out.put(t.getId()));
        return out;
    }

    @CrossOrigin()
    @JsonView(Views.Rest.class)
    @RequestMapping(path = "/parse-text", method = RequestMethod.POST)
    public PhenoMarks parseText(@RequestBody ParseTextQuery query) {

        PhenoMarks phenoMarks = phenoMarksParser.tagInput(query.getQuery());
        return phenoMarks;

    }

    @CrossOrigin()
    @RequestMapping(path = "/get-diseases", method = RequestMethod.POST, consumes = {"application/json"})
    public Set<DiseaseResult> getDiseases(@RequestBody GetGenesRequest request) {
        Set<HpoTerm> hpoTerms = this.getParsedHpoTerms(request.getHpoTerms());

        //System.out.println("Diseases " + this.hpoTree.getDiseases(hpoTerms).size());

        Map<String, Double> result = this.hpoTree.getDiseases(hpoTerms)
                .entrySet()
                .stream()
                .filter(map -> map.getValue() > request.getThreshold())
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

        //System.out.println("Result " + result.size());

        // sorting
        result = result.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));

        //System.out.println("Result " + result.size());

        Set<DiseaseResult> finalResult = new TreeSet<DiseaseResult>();
        for (Entry<String, Double> entry : result.entrySet()) {
            //System.out.println(entry.getKey());
            if (this.diseaseGeneDictionary.getDiseaseById(entry.getKey()) != null)
                finalResult.add(
                        new DiseaseResult(
                                this.diseaseGeneDictionary
                                        .getDiseaseById(entry.getKey())
                                        .getPrimaryName(),
                                100 * entry.getValue()
                        )
                );
        }
        //System.out.println("Final Result " + finalResult.size());

        return finalResult;

    }

    @CrossOrigin()
    @RequestMapping(path = "/get-genes", method = RequestMethod.POST, consumes = {"application/json"})
    public Set<GeneResult> getGenes(@RequestBody GetGenesRequest request) {
        Set<HpoTerm> hpoTerms = this.getParsedHpoTerms(request.getHpoTerms());

        // getting diseases
        Map<String, Double> result = this.hpoTree.getDiseases(hpoTerms)
                .entrySet()
                .stream()
                .filter(map -> map.getValue() > request.getThreshold())
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

        // sorting
        result = result.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));


        Set<GeneResult> finalResult = new TreeSet<GeneResult>();
        for (Entry<String, Double> entry : result.entrySet()) {
            if (this.diseaseGeneDictionary.getDiseaseById(entry.getKey()) != null)
                for (String gene : this.diseaseGeneDictionary.getDiseaseById(entry.getKey()).getGenes())
                    finalResult.add(new GeneResult(gene, 100 * entry.getValue()));

        }
        //System.out.println("Final Result " + finalResult.size());

        return finalResult;

    }

    private Set<HpoTerm> getParsedHpoTerms(Set<String> simpleHpoTerms) {
        Set<HpoTerm> parsedHpoTerms = new HashSet<HpoTerm>();
        for (String simpleHpoTerm : simpleHpoTerms)
            parsedHpoTerms.add(this.hpoTree.getHpoTermById(simpleHpoTerm));
        return parsedHpoTerms;
    }

    @CrossOrigin()
    @JsonView(Views.Rest.class)
    @RequestMapping(path = "/get-phenotypes-by-disease", method = RequestMethod.GET)
    public Set<HpoTerm> getPhenotypesByDisease(@RequestParam String disease) {

        try {
            return makeSetSpecific(hpoTree.getPhenotypesByDisease(diseaseGeneDictionary.getDiseaseByName(disease).getId()));
        } catch (NoSuchElementException e) {
            return makeSetSpecific(hpoTree.getPhenotypesByDisease(disease));
        }

    }

    @CrossOrigin()
    @JsonView(Views.Rest.class)
    @RequestMapping(path = "/disease-autocomplete", method = RequestMethod.GET)
    public Set<Object> diseasesAutocomplete(
            @RequestParam String firstLetters,
            @RequestParam Integer resultsCount) {

        Results results = new Results();

        for (String disease : this.diseaseGeneDictionary.getDiseases()) {
            if (disease.toLowerCase().startsWith(firstLetters.toLowerCase())) {
                results.addResult(disease);
                if (results.sizeGreaterOrEqualTo(resultsCount))
                    return results.getResults();
            }
        }

        for (String disease : this.diseaseGeneDictionary.getDiseases()) {
            if (disease.toLowerCase().contains(firstLetters.toLowerCase())) {
                results.addResult(disease);
                if (results.sizeGreaterOrEqualTo(resultsCount))
                    return results.getResults();
            }
        }

        return results.getResults();//phenoMarks;

    }

    @CrossOrigin()
    @JsonView(Views.Rest.class)
    @RequestMapping(path = "/get-phenotype-by-id", method = RequestMethod.GET)
    public PhenotypeResult getPhenotypeById(@RequestParam String id) {
        return new PhenotypeResult(this.hpoTree.getHpoTermById(id));
    }

    @CrossOrigin()
    @JsonView(Views.Rest.class)
    @RequestMapping(path = "/phenotype-autocomplete", method = RequestMethod.GET)
    public Set<Object> phenotypeAutocomplete(
            @RequestParam String firstLetters,
            @RequestParam Integer resultsCount) {

        Results results = new Results();

        for (HpoTerm hpoTerm : this.hpoTree.getTerms()) {
            if (hpoTerm.getName().toLowerCase().startsWith(firstLetters.toLowerCase())) {
                results.addResult(hpoTerm);
                if (results.sizeGreaterOrEqualTo(resultsCount))
                    return results.getResults();
            }
        }

        for (HpoTerm hpoTerm : this.hpoTree.getTerms()) {
            if (hpoTerm.getName().toLowerCase().contains(firstLetters.toLowerCase())) {
                results.addResult(hpoTerm);
                if (results.sizeGreaterOrEqualTo(resultsCount))
                    return results.getResults();
            }
        }

        return results.getResults();

    }

}