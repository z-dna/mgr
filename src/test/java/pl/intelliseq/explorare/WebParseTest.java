package pl.intelliseq.explorare;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.intelliseq.explorare.model.HpoTermDistancePair;
import pl.intelliseq.explorare.model.hpo.*;
import pl.intelliseq.explorare.model.hpo.results.GeneResult;
import pl.intelliseq.explorare.model.phenoMarks.PhenoMarks;
import pl.intelliseq.explorare.model.phenoMarks.PhenoMarksParser;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebParseTest {

    @Autowired
    PhenoMarksParser phenoMarksParser;

    @Autowired
    DiseaseGeneDictionary diseaseGeneDictionary;

    @Autowired
    HpoTree hpoTree;

    @Autowired
    private TestRestTemplate restTemplate;

    public JSONObject getResultsForDisease(String url, String mainGene) throws UnirestException {
        System.out.println(url);
        HttpResponse<JsonNode> response = Unirest.get(url+"?report=json").asJson();

        List<String> dbKeys = new ArrayList<>();

        String name = response.getBody().getObject().getString("name");

        JSONArray dbKeyList = response.getBody().getObject().getJSONArray("db-key-list");

        for (Object dbKey: dbKeyList) {
            if (((JSONObject)dbKey).getJSONObject("db-key").getString("db").equals("OMIM")){
                dbKeys.add("OMIM:"+((JSONObject)dbKey).getJSONObject("db-key").getString("key"));
            } else if (((JSONObject)dbKey).getJSONObject("db-key").getString("db").equals("Orphanet")){
                dbKeys.add("ORPHA:"+((JSONObject)dbKey).getJSONObject("db-key").getString("key"));
            }
        }

        String desc = response.getBody().getObject().getJSONArray("text-list").getJSONObject(0).getJSONObject("text").getString("html");

        PhenoMarks phenoMarks = phenoMarksParser.tagInput(desc);

//        System.out.println(phenoMarks.getMarkedText());

//        System.out.println(phenoMarks.getHpoTerms());

        Map<String, Double> result = hpoTree.getDiseases(phenoMarks.getHpoTerms());


        List<String> sortedKeys = result.entrySet()
                .stream()
//                .filter(map -> map.getValue() > 0.05d)
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(e -> e.getKey())
                .collect(Collectors.toList());

//        sortedKeys.forEach(k -> System.out.println(k+" : "+result.get(k)));


        Set<GeneResult> finalResult = new TreeSet<GeneResult>();

        for(String key : sortedKeys) {
            if (this.diseaseGeneDictionary.getDiseaseById(key) != null)
                for (String gene : this.diseaseGeneDictionary.getDiseaseById(key).getGenes())
                    finalResult.add(new GeneResult(gene, 100 * result.get(key)));

        }



        JSONArray topResults = new JSONArray();

        sortedKeys.subList(0,Math.min(10, sortedKeys.size())).forEach(k -> topResults.put(new JSONObject().put("name", k).put("value", result.get(k))));

        JSONArray jsonDbKeysValues = new JSONArray();

        dbKeys.forEach(k ->
                jsonDbKeysValues.put(new JSONObject().put("name", k).put("score", result.get(k)).put("place", sortedKeys.indexOf(k)))
        );

//        System.out.println(jsonDbKeysValues);

        JSONArray jsonHpoTerms = new JSONArray();

        phenoMarks.getHpoTerms().stream().map(term -> new JSONObject().put("name", term.getName()).put("id", term.getId())).forEach(jsonHpoTerms::put);


        JSONArray genes = new JSONArray();
        Integer finalRank = -1;
        Double finalScore = 0D;

        List<GeneResult> finalResultList = new LinkedList<>(finalResult);

        for (int i = 0; i < finalResultList.size(); i++) {
            if (finalResultList.get(i).getName().equals(mainGene) && finalRank == -1){
                finalRank = i;
                finalScore = finalResultList.get(i).getScore();
            }
            genes.put(new JSONObject().put("name", finalResultList.get(i).getName()).put("score", finalResultList.get(i).getScore()));
        }

        return new JSONObject()
                .put("disease-name", name)
                .put("gene-rank", finalRank)
                .put("gene-score", finalScore)
                .put("gene-diseases", new JSONArray().put(dbKeys))
                .put("gene-diseases-results", jsonDbKeysValues)
                .put("calculated-genes", genes)
                .put("marked-text", phenoMarks.getMarkedText())
                .put("hpo-terms", jsonHpoTerms);
    }


    public void test() throws UnirestException {
        String gene = "RAB3GAP1";

        JSONArray jsonGene = new JSONArray();

        HttpResponse<JsonNode> response = Unirest.get("https://ghr.nlm.nih.gov/gene/"+gene+"?report=json").asJson();
        for (Object url : response.getBody().getObject().getJSONArray("related-health-condition-list")) {
            jsonGene.put(getResultsForDisease(((JSONObject) url).getJSONObject("related-health-condition").getString("ghr-page"), gene));
        }

        JSONObject out = new JSONObject().put(gene, jsonGene);

        System.out.println(out);
    }

    public List<String> getRandomGenes(Integer size) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("genelist").getFile());

        BufferedReader bufferedreader = new BufferedReader(new FileReader(file));
        String line = bufferedreader.readLine();

        List<String> genes = new ArrayList<>();

        while (line != null) {
            genes.add(line);
            line = bufferedreader.readLine();
        }

        Collections.shuffle(genes);
        return genes.subList(0, size);

    }

    public JSONObject getResultForGene(String gene) {
        JSONArray jsonGene = new JSONArray();

        HttpResponse<JsonNode> response = null;
        try {
            response = Unirest.get("https://ghr.nlm.nih.gov/gene/"+gene+"?report=json").asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
            return null;
        }
        for (Object url : response.getBody().getObject().getJSONArray("related-health-condition-list")) {
            try {
                jsonGene.put(getResultsForDisease(((JSONObject) url).getJSONObject("related-health-condition").getString("ghr-page"), gene));
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        }

        return new JSONObject().put("name", gene).put("results", jsonGene);
    }




    public void hTest() throws IOException {
        List<String> randomGenes = getRandomGenes(3);

        JSONArray out = new JSONArray();

        randomGenes.forEach(g -> out.put(getResultForGene(g)));

        System.out.println(out);

    }


    public void parseAll() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("genelist").getFile());

        BufferedReader bufferedreader = new BufferedReader(new FileReader(file));
        String line = bufferedreader.readLine();

        List<String> genes = new ArrayList<>();

        while (line != null) {
            genes.add(line);
            line = bufferedreader.readLine();
        }

//        List<String> smallList = genes.subList(0,1);

        for (String gene: genes){
            System.out.println(gene);
            File writeFile = new File("src/main/resources/results/genes/"+gene);
            if(!writeFile.exists()){
                writeFile.createNewFile();
                JSONObject result = getResultForGene(gene);
                if (result != null) {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(writeFile));
                    writer.write(result.toString());
                    writer.flush();
                    writer.close();
                } else {
                    writeFile.delete();
                }
            }
        }

    }


    public void singleTest() {
        System.out.println("Single test");
        JSONObject out = getResultForGene("ISPD");
        System.out.println(out);

    }


    public void singleTestWithSaveToFile() throws IOException {
        List<String> genes = List.of("ACAD8", "GP6", "CLPP", "FOXL2", "HARS2", "HSD17B4", "LARS2", "TWNK", "ELANE", "HAX1", "TCIRG1", "WAS", "AAAS", "ELP1", "LIFR", "CASQ2", "RYR2", "COL18A1");

        for(String gene:genes) {

            JSONObject out = getResultForGene(gene);

            File writeFile = new File("src/main/resources/results/genes/" + gene);
            BufferedWriter writer = new BufferedWriter(new FileWriter(writeFile));
            writer.write(out.toString());
            writer.flush();
            writer.close();
        }

    }


    public void stats() throws FileNotFoundException {
        File dir = new File("src/main/resources/results/genes/");
        File[] files = dir.listFiles();

        System.out.println(files.length);

        Integer notFoundCount = 0;
        Integer emtyCount = 0;

        for(File file:files){
            InputStream is = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(is);
            JSONObject jsonObject = new JSONObject(tokener);

            JSONArray results = jsonObject.getJSONArray("results");
            String geneName = jsonObject.getString("name");

            Boolean notFound = true;
            Boolean empty = true;

            for(Object result : results) {
                Integer rank = (Integer) ((JSONObject)result).get("gene-rank");
                if (rank >= 0){
                    notFound = false;
                }
                Double score =  ((JSONObject)result).getDouble("gene-score");
                JSONArray terms = ((JSONObject)result).getJSONArray("hpo-terms");
                if (terms.length() > 0){
                    empty = false;
                }
//                System.out.printf("%d : %f\n", rank, score);
            }

            if(empty){
                emtyCount++;
                System.out.println(geneName+" empty");
            }
            if (notFound) {
                notFoundCount++;
                System.out.println(geneName+" not found");
            }
        }

        System.out.printf("Out of %d genes %d have not hpo terms and %d was not classified properly\n", files.length, emtyCount, notFoundCount);

//        System.out.println(Arrays.toString(files));
    }


    public Map<String, List> statsRanksFromFile() throws FileNotFoundException {
        File dir = new File("src/main/resources/results/genes/");
        File[] files = dir.listFiles();

        System.out.println(files.length);

        List<Integer> rankList = new ArrayList<>();
        List<Double> scoreList = new ArrayList<>();

        Map<String, List> resultMap = new HashMap<>();
        resultMap.put("rankList", rankList);
        resultMap.put("scoreList", scoreList);

        for(File file:files){
            InputStream is = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(is);
            JSONObject jsonObject = new JSONObject(tokener);

            JSONArray results = jsonObject.getJSONArray("results");

            for(Object result : results) {
                Integer rank = (Integer) ((JSONObject)result).get("gene-rank");
                Double score =  ((JSONObject)result).getDouble("gene-score");
                rankList.add(rank);
                scoreList.add(score);

            }
        }
        return resultMap;
    }

    public JSONObject getRankAndScore(Object object, String mainGene){
        JSONArray hpoTermsJson = ((JSONObject) object).getJSONArray("hpo-terms");
        Set<HpoTerm> hpoTerms = new HashSet<>();
        hpoTermsJson.forEach(t -> hpoTerms.add(hpoTree.getHpoTermById(((JSONObject)t).getString("id"))));

        Map<String, Double> result = hpoTree.getDiseases(hpoTerms);

//        System.out.println("Test: "+result.get("OMIM:601076"));

        List<String> sortedKeys = result.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(e -> e.getKey())
                .collect(Collectors.toList());

        Set<GeneResult> finalResult = new TreeSet<GeneResult>();

        for(String key : sortedKeys) {
            if (this.diseaseGeneDictionary.getDiseaseById(key) != null)
                for (String gene : this.diseaseGeneDictionary.getDiseaseById(key).getGenes())
                    finalResult.add(new GeneResult(gene, 100 * result.get(key)));

        }

        JSONArray topResults = new JSONArray();

        sortedKeys.subList(0,Math.min(10, sortedKeys.size())).forEach(k -> {
//            System.out.println("put: "+k+" "+result.get(k));
            topResults.put(new JSONObject().put("name", k).put("value", result.get(k)));
        });


        JSONArray jsonHpoTerms = new JSONArray();

        hpoTerms.stream().map(term -> new JSONObject().put("name", term.getName()).put("id", term.getId())).forEach(jsonHpoTerms::put);


        JSONArray genes = new JSONArray();
        Integer finalRank = -1;
        Double finalScore = 0D;

        List<GeneResult> finalResultList = new LinkedList<>(finalResult);

        for (int i = 0; i < finalResultList.size(); i++) {
            if (finalResultList.get(i).getName().equals(mainGene) && finalRank == -1){
                finalRank = i;
                finalScore = finalResultList.get(i).getScore();
            }
            genes.put(new JSONObject().put("name", finalResultList.get(i).getName()).put("score", finalResultList.get(i).getScore()));
        }

        return new JSONObject()
                .put("gene-rank", finalRank)
                .put("gene-score", finalScore);
    }

    public Map<String, List> statsRanks() throws FileNotFoundException {
        File dir = new File("src/main/resources/results/genes/");
        File[] files = dir.listFiles();

        System.out.println(files.length);

        List<Integer> rankList = new ArrayList<>();
        List<Double> scoreList = new ArrayList<>();

        Map<String, List> resultMap = new HashMap<>();
        resultMap.put("rankList", rankList);
        resultMap.put("scoreList", scoreList);

        for(File file:files){
            InputStream is = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(is);
            JSONObject jsonObject = new JSONObject(tokener);

            String mainGene = jsonObject.getString("name");

            JSONArray results = jsonObject.getJSONArray("results");

            for(Object result : results) {
                JSONObject rankAndScore = getRankAndScore(result, mainGene);
                Integer rank = rankAndScore.getInt("gene-rank");
                Double score =  rankAndScore.getDouble("gene-score");
                rankList.add(rank);
                scoreList.add(score);

            }
        }
        return resultMap;
    }

    @Test
    public void rankScoreManager() throws FileNotFoundException {
        Map<String, List> map = statsRanks();
        List<Integer> rankList = map.get("rankList");
        List<Double> scoreList = map.get("scoreList");
        Collections.sort(rankList);
        Collections.sort(scoreList);

        Function<Integer, Integer> rankFunc = integer -> {
            if (integer == -1){
                return -1;
            }
            if (integer == 0){
                return 1;
            }
            if(integer < 10){
                return 10;
            }
            if(integer < 100){
                return 100;
            }
            if(integer < 1000){
                return 1000;
            }
            if(integer < 10000){
                return 10000;
            }
            return -1;

        };

        Function<Double, Integer> scoreFunc = aDouble -> ((int) (Math.ceil(aDouble)));

        System.out.println("Rank List: \n"+rankList);
        Map<Integer, Long> rankGroups =
                rankList.stream().collect(Collectors.groupingBy(rankFunc, Collectors.counting()));
        System.out.println("Rank groups: \n"+rankGroups);

        System.out.println("Score List: \n"+scoreList);
        Map<Integer, Long> scoreGroups = scoreList.stream().collect(Collectors.groupingBy(scoreFunc, Collectors.counting()));
        System.out.println("Score Groups: \n"+scoreGroups);

        System.out.println(new JSONObject().put("rank", new JSONObject(rankGroups)).put("score", new JSONObject(scoreGroups)).put("desc", "desc"));

        Long t1 = rankGroups.get(1);
        Long t10 = t1 + rankGroups.get(10);
        Long t100 = t10 + rankGroups.get(100);
        Long t1000 = t100 + rankGroups.get(1000);
        Long t10000 = t1000 + rankGroups.get(10000);
        Long tmax = t10000 + rankGroups.get(-1);

        System.out.printf("\n###\t%s\t###\n", Setup.getDesc());

        System.out.printf("# Cumulative rank list starts with rank 1\nc(%d, %d, %d, %d, %d, %d) # %s\n", t1, t10, t100, t1000, t10000, tmax, Setup.getDesc());

        List<Long> cumulativeScoreList = new LinkedList();

        Long cumulativeScore = scoreGroups.get(100);
        cumulativeScoreList.add(cumulativeScore);

        StringBuilder scoreStringBuilder = new StringBuilder("# Cumulative score list starts with score 100\nc(").append(scoreGroups.get(100));
        for(int i=99; i >= 0; i--){
            scoreStringBuilder.append(", ");
            cumulativeScore += scoreGroups.get(i) != null ? scoreGroups.get(i) : 0;
            cumulativeScoreList.add(cumulativeScore);
            scoreStringBuilder.append(cumulativeScore);
        }
        scoreStringBuilder.append(") # ").append(Setup.getDesc()).append("\n");
        System.out.print(scoreStringBuilder.toString());


        //AUC
        Double cumulativeRankScore = (t1+t100)/2D+t10;
//        System.out.println(cumulativeRankScore);
//        System.out.println(tmax*5);
        System.out.printf("# Rank AOC top100: \n%s # %s\n", cumulativeRankScore/(t100*2), Setup.getDesc());

        cumulativeRankScore = (t1+t1000)/2D+t10+t100;
//        System.out.println(cumulativeRankScore);
//        System.out.println(tmax*5);
        System.out.printf("# Rank AOC top1000: \n%s # %s\n", cumulativeRankScore/(t1000*3), Setup.getDesc());

        cumulativeRankScore = (t1+tmax)/2D+t10+t100+t1000+t10000;
//        System.out.println(cumulativeRankScore);
//        System.out.println(tmax*5);
        System.out.printf("# Rank AOC all: \n%s # %s\n", cumulativeRankScore/(tmax*5), Setup.getDesc());


        Double cumulativeScoreSum = 0D;
        for (int i = 0; i < cumulativeScoreList.size() - 1; i++) {
//            System.out.println(i);
//            System.out.println(cumulativeScoreList.get(i)+ " " + cumulativeScoreList.get(i+1));
            cumulativeScoreSum += (cumulativeScoreList.get(i)+cumulativeScoreList.get(i+1))/2D;
        }
//        System.out.println(cumulativeScoreSum);
//        System.out.println(cumulativeScoreList.get(cumulativeScoreList.size() - 1)*(cumulativeScoreList.size() - 1));
        System.out.printf("# Score AOC: \n%s # %s\n\n", cumulativeScoreSum/(cumulativeScoreList.get(cumulativeScoreList.size() - 1)*(cumulativeScoreList.size() - 1)), Setup.getDesc());

    }

    @Test
    public void pathToRoot(){
        Map<String, Integer> t1 = hpoTree.getHpoTermById("HP:0003698").getPathToRoot(0, new HashMap<>());
        Map<String, Integer> t2 = hpoTree.getHpoTermById("HP:0012125").getPathToRoot(0, new HashMap<>());


        System.out.println(t1);
        System.out.println(t2);

        Integer min = Integer.MAX_VALUE;
        String key = null;
        for (String term: t1.keySet()){
            if(t2.containsKey(term)){
                if(t1.get(term)+ t2.get(term) < min){
                    min = t1.get(term)+ t2.get(term);
                    key = term;
                }
            }
        }
        System.out.println(min);
        System.out.println(key+" "+hpoTree.getHpoTermById(key).getName());
    }

    @Test
    public void distTest(){
        HpoTerm t1 = hpoTree.getHpoTermById("HP:0031792");
        HpoTerm t2 = hpoTree.getHpoTermById("HP:0000622");

        System.out.println(t1.getDistFromOther(t2));
    }

    @Test
    public void geneDistTest() throws FileNotFoundException {
        String gene = "BRCA1";
        File file = new File("src/main/resources/results/genes/"+gene);

        InputStream is = new FileInputStream(file);
        JSONTokener tokener = new JSONTokener(is);
        JSONObject jsonObject = new JSONObject(tokener);

        String mainGene = jsonObject.getString("name");

        JSONArray results = jsonObject.getJSONArray("results");

        for(Object result : results) {
            List<HpoTerm> terms = new ArrayList<>();
            Set<HpoTermDistancePair> pairs = new LinkedHashSet<>();
           ((JSONObject) result).getJSONArray("hpo-terms").forEach(o -> terms.add(hpoTree.getHpoTermById((String) ((JSONObject)o).get("id"))));
            for(HpoTerm t1: terms){
                System.out.println(t1+" "+t1.getWeight());
                for (HpoTerm t2: terms){
                    if(t1!=t2 && !pairs.contains(new HpoTermDistancePair(t1, t2, 0))){
                        pairs.add(new HpoTermDistancePair(t1,t2, t1.getDistFromOther(t2)));
                    }
                }
            }
            System.out.println(pairs);

        }

    }

    @Test
    public void getSingleDistTest() throws FileNotFoundException {
        String gene = "BRCA1";
        File file = new File("src/main/resources/results/genes/"+gene);

        InputStream is = new FileInputStream(file);
        JSONTokener tokener = new JSONTokener(is);
        JSONObject jsonObject = new JSONObject(tokener);

        JSONObject result = (JSONObject) jsonObject.getJSONArray("results").get(2);

        List<HpoTerm> terms = new ArrayList<>();
        Set<HpoTermDistancePair> pairs = new LinkedHashSet<>();

        Map<HpoTerm, Double> scaledDistMap = new LinkedHashMap<>();

        result.getJSONArray("hpo-terms").forEach(o -> terms.add(hpoTree.getHpoTermById((String) ((JSONObject)o).get("id"))));
        Double globalSum = 0D;
        for(HpoTerm t1: terms){
            Double localSum = 0D;
            for (HpoTerm t2: terms){
                if(t1!=t2){
                    localSum += t1.getDistFromOther(t2);
                    pairs.add(new HpoTermDistancePair(t1,t2, t1.getDistFromOther(t2)));
                }
            }
            scaledDistMap.put(t1, localSum);
            System.out.println(t1+" w: "+t1.getWeight()+" d: "+localSum);
            globalSum += localSum;
        }
        System.out.println(globalSum + " " + globalSum/terms.size());

        globalSum = globalSum / 6;

        for(HpoTerm t1: terms){
//            System.out.println(t1+" scaled dist: "+scaledDistMap.get(t1)/globalSum+" "+);
            System.out.println(String.format("ID: %s,\tscaled dist: %.2f,\treversed dist: %.2f", t1.getId(), scaledDistMap.get(t1)/globalSum, globalSum/scaledDistMap.get(t1)));
        }
        System.out.println(pairs);

    }

    @Test
    public void getChildren(){
        String url = "/get-children?id=HP:0000539";
        String body = this.restTemplate.getForObject(url, String.class);
        System.out.println(body);
    }

//    @Test
//    public void multiRankScoreManager() throws FileNotFoundException {
//        JSONObject out = new JSONObject();
//        for (int i = 0; i <= 50; i++) {
//            Double exp = (i/25d - 1)*Math.E;
//            hpoTree.calculateWeights(exp);
//
//            Map<String, List> map = statsRanks();
//            List<Integer> rankList = map.get("rankList");
//            List<Double> scoreList = map.get("scoreList");
//            Collections.sort(rankList);
//            Collections.sort(scoreList);
//
//            Function<Integer, Integer> rankFunc = integer -> {
//                if (integer == -1) {
//                    return -1;
//                }
//                if (integer == 0) {
//                    return 1;
//                }
//                if (integer < 10) {
//                    return 10;
//                }
//                if (integer < 100) {
//                    return 100;
//                }
//                if (integer < 1000) {
//                    return 1000;
//                }
//                if (integer < 10000) {
//                    return 10000;
//                }
//                return -1;
//
//            };
//
////            Function<Double, Integer> scoreFunc = aDouble -> ((int) (Math.floor(aDouble)));
//
//            System.out.println(rankList);
//            Map<Integer, Long> rankGroups =
//                    rankList.stream().collect(Collectors.groupingBy(rankFunc, Collectors.counting()));
//            System.out.println(rankGroups);
//
////            Map<Integer, Long> scoreGroups = scoreList.stream().collect(Collectors.groupingBy(scoreFunc, Collectors.counting()));
////            System.out.println(scoreGroups);
//
//            out.put(exp.toString(), new JSONObject()
//                    .put("rank", new JSONObject(rankGroups))
////                    .put("score", new JSONObject(scoreGroups))
////                    .put("desc", "desc")
//            );
//        }
//        System.out.println(out);
//
//    }
}
